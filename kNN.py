from mnist.loader import MNIST
from operator import itemgetter
import numpy as np


class Classificator:
    def __init__(self):
        loader = MNIST('./dataset')
        loader.load_training()
        loader.load_testing()
        #         loader.load_test()
        self.train_images = np.array(loader.train_images)
        self.train_labels = np.array(loader.train_labels)
        self.test_images = loader.test_images
        self.test_labels = loader.test_labels
        self.train_N = 10000

    def classify(self, img, k=0.001):
        i_class = np.zeros(10)
        N = self.train_N
        kNN = int(N * k)
        k_mass = []
        mass_class = np.zeros(10)
        for j in range(1, N):
            dist = np.sqrt(np.power(img - self.train_images[j], 2).sum())

            k_mass.append([dist, self.train_labels[j]])

        k_mass = sorted(k_mass, key=itemgetter(0))

        for k in range(kNN):
            i_class[k_mass[k][1]] += 1 / k_mass[k][0]
            mass_class[k_mass[k][1]] += 1

        i_res = i_class.argmax()
        #         print(mass_class)
        return i_res, (mass_class[i_res] / mass_class.sum())

    def test(self):
        right = 0
        N = 100
        for i in range(self.train_N, self.train_N + N):
            if self.classify(self.test_images[i])[0] == self.test_labels[i]:
                right += 1

        return right / N
