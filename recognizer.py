from kNN import Classificator
import cv2
import numpy as np
import matplotlib.pyplot as plt
from f1 import F1_score

img = cv2.imread('./imgs/1.jpg')
img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
# imshow(img)
print(type(img), img.shape)
img2 = np.copy(img)
img2 = cv2.Canny(img, 1000, 150)
kernel = np.ones((3, 3), np.uint8)

close_img = cv2.morphologyEx(img2, cv2.MORPH_CLOSE, kernel)

test = np.copy(close_img)
im2, contours, hierarchy = cv2.findContours(test, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
print(len(contours))

# contours = np.array(filter(lambda x: len(x) > 4, contours))
norm_conts = []
print(len(contours[100]))
for (i, cont) in enumerate(contours):
    if len(cont) > 60:
        norm_conts.append(cont)
        if i == 100:
            print('new ind: ', len(norm_conts))

contours = norm_conts
print(len(contours))
mas_ax = []


# fig, ((ax1, ax2)) = plt.subplots(nrows=1, ncols=2)
# figg, mas_ax = plt.subplots()


def to28x28(contour):
    cont = np.copy(contour)
    mins = np.min(cont, axis=0)
    cont -= np.full(shape=(len(cont), 1, 2), fill_value=mins)
    shape = np.max(cont, axis=0)[0]

    dig = np.zeros((shape[1], shape[0]), np.uint8)

    cv2.drawContours(dig, [cont], -1, (255, 139, 0), 2)

    return dig


def scaling(pic):
    h, w = pic.shape
    addit = int(max(h, w) / 3)
    #     print(h, w, addit, int((h - w)/2))
    if h > w:
        zeros = np.zeros((h, int((h - w) / 2)), np.uint8)
        p2 = np.concatenate((zeros, pic, zeros), axis=1)
        w += int((h - w) / 2) * 2
    else:
        zeros = np.zeros((int((w - h) / 2), w), np.uint8)
        p2 = np.concatenate((zeros, pic, zeros), axis=0)
        h += int((w - h) / 2) * 2

    zeros = np.zeros((h, addit), np.uint8)
    p2 = np.concatenate((zeros, p2, zeros), axis=1)
    zeros = np.zeros((addit, w + 2 * addit), np.uint8)
    p2 = np.concatenate((zeros, p2, zeros), axis=0)

    p2 = cv2.resize(p2, (28, 28), interpolation=cv2.INTER_CUBIC)

    return p2



ind = 99
cl = Classificator()
# picture = to28x28(contours[ind])
# resized_picture = scaling(picture).reshape((28*28))
# print(cl.classify(resized_picture))
# print(len(contours[ind]))
f1_score = F1_score(np.concatenate(cl.test_images, cl.train_images), np.concatenate(cl.test_labels, cl.train_labels))
f1_score.f1_score(cl.classify)

