from collections import namedtuple
from random import random, choice
from time import time
import numpy as np
from mnist.loader import MNIST


class F1_score:
    testing_data = {}
    training_data = {}
    distance_function = np.vectorize(lambda x, y: 1 if bool(x) ^ bool(y) else 0)

    def __init__(self, imgs, lbls):

        start = time()
        for x, y in zip(imgs, lbls):

            if random() < 0.2:
                set = self.testing_data.get(y, [])
                set.append(np.array(x))
                self.testing_data[y] = set

            else:
                set = self.training_data.get(y, [])
                set.append(np.array(x))
                self.training_data[y] = set
        print(time() - start)

    def f1_score(self, class_function):

        summary_Fscore = 0

        for expected_ans in self.testing_data:
            true_positive = 0
            false_positive = 0
            all_positive = len(self.testing_data[expected_ans])

            for true_ans in self.testing_data:
                for x in self.testing_data[true_ans]:

                    algorithm_ans = class_function(x)

                    if algorithm_ans == expected_ans and algorithm_ans == true_ans:
                        true_positive += 1

                    elif algorithm_ans == expected_ans and algorithm_ans != true_ans:
                        false_positive += 1

            if true_positive + false_positive == 0:
                precision = 0
            else:
                precision = true_positive / true_positive + false_positive

            recall = true_positive / all_positive

            if precision + recall == 0:
                Fscore = 0
            else:
                Fscore = 2 * (precision * recall) / (precision + recall)
            summary_Fscore += Fscore

        return summary_Fscore / len(self.testing_data)
